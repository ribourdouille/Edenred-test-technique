import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../utils/ws.dart';

class CartBloc extends Cubit<int> {
  final _streamController = StreamController<Map<String, dynamic>>();
  Sink<Map<String, dynamic>> get sink => _streamController.sink;
  Stream<Map<String, dynamic>> get streamS => _streamController.stream;

  laodData() async {
    Map<String, dynamic> map = await WS().getDataCart();
    sink.add(map);
  }

  CartBloc() : super(0);

  dispose() {
    _streamController.close();
  }
}
