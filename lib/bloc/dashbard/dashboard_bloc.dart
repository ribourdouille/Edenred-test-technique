import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../model/book.dart';
import '../../utils/ws.dart';

class DashboardBloc extends Cubit<int> {
  final _streamController = StreamController<List<Book>>();
  Sink<List<Book>> get sink => _streamController.sink;
  Stream<List<Book>> get streamS => _streamController.stream;

  laodData() async {
    List<Book> list = await WS().getBooks();
    sink.add(list);
  }

  getListFiltred(String val) async{
    List<Book> temp = await WS().getBooks();
    List<Book> list = [];
    for (var element in temp) {
      if(element.title.contains(val)){
        list.add(element);
      }
    }
    sink.add(list);
  }

  DashboardBloc() : super(0);

  dispose() {
    _streamController.close();
  }
}
