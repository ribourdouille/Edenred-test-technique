import 'package:edenred/model/cart_item.dart';
import 'package:edenred/ui/dashboard_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'bloc/dashbard/dashboard_bloc.dart';

Future<void> main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CartItemAdapter());
  await Hive.openBox<CartItem>('cart');
  await Hive.openBox<int>('nbBook');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Henri Potier'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Box<CartItem> dataBox;
  late Box<int> nbBook;


  @override
  void initState() {
    super.initState();
    dataBox = Hive.box<CartItem>('cart');
    nbBook = Hive.box<int>('nbBook');
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DashboardBloc>(
        create: (BuildContext context) => DashboardBloc(),
        child: DashboardView(dataBox: dataBox, nbBook: nbBook,));
  }
}
