class Book {

  String isbn;
  String title;
  int price;
  String cover;
  String synopsis;

  Book.fromJson(Map json):
        isbn = json["isbn"],
        title = json["title"],
        price = json["price"],
        cover = json["cover"],
        synopsis = json["synopsis"][0]
  ;

}