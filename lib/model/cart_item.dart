import 'package:hive/hive.dart';

part 'cart_item.g.dart';

@HiveType(typeId: 0)
class CartItem extends HiveObject {
  @HiveField(0)
  String? isbn;

  @HiveField(1)
  String? title;

  @HiveField(2)
  int? price;

  @HiveField(3)
  String? cover;

  @HiveField(4)
  int qty;

  CartItem({this.isbn, this.title, this.price, this.cover, required this.qty});
}
