import 'package:cached_network_image/cached_network_image.dart';
import 'package:edenred/model/cart_item.dart';
import 'package:edenred/utils/alert_helper.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../model/book.dart';

class BookView extends StatelessWidget {
  final Book book;
  final Box<CartItem> dataBox;
  final Box<int> nbBook;
  const BookView({Key? key,required this.book, required this.dataBox, required this.nbBook}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        CartItem? cartItem = dataBox.get(book.isbn);
        if(cartItem != null){
          cartItem.qty += 1;
        }else{
          cartItem = CartItem(
              title: book.title,
              price: book.price,
              isbn: book.isbn,
              cover: book.cover,
              qty: 1
          );
        }

        dataBox.put(book.isbn, cartItem);
        nbBook.values.isNotEmpty ?
        nbBook.putAt(0, nbBook.values.first + 1)
        : nbBook.add(cartItem.qty);

        AlertHelper().goCart(context, dataBox, nbBook);

      },
      child: Container(
        padding: const EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width/3,
              height: (MediaQuery.of(context).size.width/3) + 50,
              decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(book.cover),
                ),
              ),
            ),
            const SizedBox(width: 10,),
            Expanded(
              child: Column(
                children: [
                  Text(
                    book.title,
                    style: const TextStyle(
                        color: Colors.blue,
                        fontSize: 18
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    book.synopsis,
                    maxLines: 6,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
