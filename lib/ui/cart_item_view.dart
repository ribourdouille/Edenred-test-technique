import 'package:cached_network_image/cached_network_image.dart';
import 'package:edenred/model/cart_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';

import '../bloc/cart/cart_bloc.dart';

class CartItemView extends StatelessWidget {
  final Box<CartItem> dataBox;
  final Box<int> nbBook;
  final CartItem cartItem;
  const CartItemView({Key? key,required this.cartItem, required this.dataBox, required this.nbBook}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CartBloc>(context);
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
    child: Container(
      padding: const EdgeInsets.all(5),
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width/3,
            height: (MediaQuery.of(context).size.width/3) + 50,
            decoration: BoxDecoration(
              color: Colors.transparent,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(cartItem.cover!),
              ),
            ),
          ),
          const SizedBox(width: 10,),
          Expanded(
            child: Column(
              children: [
                Text(
                  cartItem.title!,
                  style: const TextStyle(
                      color: Colors.blue,
                      fontSize: 18
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  cartItem.price.toString() + " €",
                ),
                const SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildOutlineButton(
                      icon: Icons.remove,
                      press: () async {
                        cartItem.qty-=1;
                        (cartItem.qty > 0) ? dataBox.put(cartItem.isbn, cartItem) : dataBox.delete(cartItem.isbn);
                        nbBook.putAt(0, nbBook.values.first -1);
                        bloc.laodData();
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20 / 2),
                      child: Text(
                        // if our item is less  then 10 then  it shows 01 02 like that
                        cartItem.qty.toString().padLeft(2, "0"),
                        style: const TextStyle(
                          color: Colors.green,
                          fontSize: 18
                        ),
                      ),
                    ),
                    buildOutlineButton(
                        icon: Icons.add,
                        press: () async {
                          cartItem.qty+=1;
                          dataBox.put(cartItem.isbn, cartItem);
                          nbBook.putAt(0, nbBook.values.first +1);
                          bloc.laodData();
                        }),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    ),
    );
  }
}

SizedBox buildOutlineButton({required IconData icon, required VoidCallback press}) {
  return SizedBox(
    width: 25,
    height: 25,
    child: OutlinedButton(
      style: OutlinedButton.styleFrom(
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(13),
        ),
      ),
      onPressed: press,
      child: Icon(
        icon,
        color: Colors.grey,
      ),
    ),
  );
}
