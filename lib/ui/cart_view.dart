import 'package:edenred/bloc/cart/cart_bloc.dart';
import 'package:edenred/ui/cart_item_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive_flutter/adapters.dart';

import '../model/cart_item.dart';
import '../utils/alert_helper.dart';

class CartView extends StatelessWidget {
  final Box<CartItem> dataBox;
  final Box<int> nbBook;
  const CartView({Key? key, required this.dataBox, required this.nbBook}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Panier"),
        centerTitle: true,
        elevation: 0,
        actions: [
          InkWell(
            onTap: () {
              AlertHelper().razClient(context, dataBox, nbBook);
            },
            child: Container(
                padding: const EdgeInsets.only(right: 10),
                child: const Icon(
                  FontAwesomeIcons.trash,
                  color: Colors.red,
                  size: 16,
                )),
          ),
        ],
      ),
      body: body(context),
    );
  }

  Widget body(BuildContext context) {
    final bloc = BlocProvider.of<CartBloc>(context);
    return dataBox.values.isNotEmpty
     ?Stack(
      children: [
        Container(
          color: Colors.white,
          child: InkWell(
            onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: Column(
              children: [
                Flexible(
                  child: SingleChildScrollView(
                    child: ValueListenableBuilder(
                        valueListenable: dataBox.listenable(),
                        builder: (context, Box<CartItem> items, _){
                          return ListView.separated(
                            separatorBuilder: (_, index) => const Divider(),
                            itemCount: items.keys.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder:(_, index){
                              final CartItem cartItem = items.getAt(index)!;
                              return CartItemView(cartItem: cartItem, dataBox: dataBox, nbBook: nbBook,);
                            },
                          );
                        }),
                  ),
                ),
                StreamBuilder(
                    stream: bloc.streamS,
                    builder: (BuildContext context, AsyncSnapshot snap) {
                      if (!snap.hasData) {
                        bloc.laodData();
                        return const Center(child: CircularProgressIndicator(),);
                      } else{
                        Map<String, dynamic> res = snap.data;
                        return Container(
                          padding: const EdgeInsets.only(bottom: 15),
                          width: MediaQuery.of(context).size.width,
                          child: Card(
                            elevation: 10,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                   Text(
                                    "Nombre total de produits (${res["nb"].toString()})",
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Coût total",
                                      ),
                                      Text(
                                        "${res["total_price"].toStringAsFixed(2)} €",
                                        style: const TextStyle(
                                            fontSize: 18,
                                            color: Colors.blue,
                                            fontWeight: FontWeight.bold),
                                      )
                                      ,

                                    ],
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: SizedBox(
                                      height: 20,
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceBetween,
                                        children: [
                                          const Text(
                                            "Avec remise de",
                                          ),
                                          Text(
                                            '${res["discount"].toStringAsFixed(2)} €',
                                          ),
                                        ],
                                      ),
                                    ),
                                  )

                                ],
                              ),
                            ),
                          ),
                        );
                      }
                    }
                ),
              ],
            ),
        ),
        )
      ],
    )
    : const Center(child: Text("Votre panier est vide"),);
  }
}
