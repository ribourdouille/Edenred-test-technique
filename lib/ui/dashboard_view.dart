import 'package:badges/badges.dart';
import 'package:edenred/ui/book_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive_flutter/adapters.dart';

import '../bloc/cart/cart_bloc.dart';
import '../bloc/dashbard/dashboard_bloc.dart';
import '../model/book.dart';
import '../model/cart_item.dart';
import 'cart_view.dart';

class DashboardView extends StatefulWidget {
  final Box<CartItem> dataBox;
  final Box<int> nbBook;
  const DashboardView({Key? key, required this.dataBox, required this.nbBook}) : super(key: key);

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {

  double _width = 0;
  String search = "";
  final double _height = 25;
  bool sortir = true;

  late Box<int> nbBook;
  late Box<CartItem> dataBox;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nbBook = widget.nbBook;
    dataBox = widget.dataBox;
  }

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<DashboardBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Henri Potier"),
        actions: [
          SizedBox(
            width: 30,
            child: IconButton(
                onPressed: () {
                  setState(() {
                    if (sortir) {
                      _width = MediaQuery.of(context).size.width - 125;
                    } else {
                      _width = 0;
                    }
                    sortir = !sortir;
                  });
                },
                icon: const Icon(
                  FontAwesomeIcons.search,
                  size: 18,
                )),
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 200),
            padding: const EdgeInsets.symmetric(vertical: 5),
            width: _width,
            height: _height,

            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                color: const Color(0xFFF6F6F6),
                borderRadius: BorderRadius.circular(25),
              ),
              child: TextFormField(
                controller: _controller,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  focusColor: Colors.white,
                  hintText: "Chercher...",
                ),
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                onChanged: (val) {
                  if (val != "") {
                    setState(() {
                      bloc.getListFiltred(val);
                      search = val;
                    });
                  } else {
                    setState(() {
                      search = "";
                    });
                    bloc.getListFiltred(val);
                  }
                },
              ),
            ),
          ),
          ValueListenableBuilder(
            valueListenable: nbBook.listenable(),
            builder: (context, box, widget) {
              return (nbBook.values.isNotEmpty)
                  ?Badge(
                badgeContent: Text(
                  nbBook.values.first.toString(),
                  style: const TextStyle(color: Colors.white, fontSize: 10),
                ),
                position: BadgePosition.topEnd(top: 10, end: 0),
                toAnimate: true,
                animationType: BadgeAnimationType.scale,
                animationDuration: const Duration(milliseconds: 100),
                child: IconButton(
                    icon: const Icon(FontAwesomeIcons.shoppingBasket),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  BlocProvider<CartBloc>(
                                      create: (BuildContext context) =>
                                          CartBloc(),
                                      child: CartView(dataBox: dataBox, nbBook: nbBook,))));
                    }),
              )
                  :IconButton(
                  icon: const Icon(FontAwesomeIcons.shoppingBasket),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                BlocProvider<CartBloc>(
                                    create: (BuildContext context) =>
                                        CartBloc(),
                                    child: CartView(dataBox: dataBox, nbBook: nbBook,))));
                  });
            },
          ),
          const SizedBox(
            width: 5,
          ),
        ],
      ),
      body: StreamBuilder(
          stream: bloc.streamS,
          builder: (BuildContext context, AsyncSnapshot snap) {
            if (!snap.hasData) {
              bloc.laodData();
              return const Center(child: CircularProgressIndicator(),);
            } else{
              List<Book> list = snap.data;
              return SingleChildScrollView(
                child: Column(
                  children: list.map((book)=> BookView(book: book, dataBox: dataBox, nbBook:  nbBook,) ).toList(),
                ),
              );
            }
          }
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
