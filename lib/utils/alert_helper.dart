import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';

import '../bloc/cart/cart_bloc.dart';
import '../model/cart_item.dart';
import '../ui/cart_view.dart';

class AlertHelper {

  TextButton close(BuildContext ctx, String text) {
    return TextButton(
      onPressed: (() => Navigator.pop(ctx)),
      child: Text(
        text,
        style: const TextStyle(color: Colors.red),
      ),
    );
  }

  Future<void> razClient(
      BuildContext context,
      Box<CartItem> dataBox,
      Box<int> nbBook
      ) async {
    Text title = const Text(
      "Vider le panier ?",
    );
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext ctx) {
          return (Theme.of(context).platform == TargetPlatform.iOS)
              ? CupertinoAlertDialog(
            title: title,
            actions: <Widget>[
              close(ctx, "NON"),
              razBtnClient(ctx, dataBox, nbBook)
            ],
          )
              : AlertDialog(
            title: title,
            actions: <Widget>[
              close(ctx, "NON"),
              razBtnClient(ctx, dataBox, nbBook)
            ],
          );
        });
  }

  TextButton razBtnClient(BuildContext ctx, Box<CartItem> dataBox, Box<int> nbBook) {
    return TextButton(
      onPressed: () async {
        dataBox.clear();
        nbBook.putAt(0, 0);
        Navigator.pop(ctx);
        Navigator.pop(ctx);
      },
      child: const Text(
        "OUI",
        style: TextStyle(color: Colors.blue),
      ),
    );
  }

  Future<void> goCart(
      BuildContext context, Box<CartItem> dataBox, Box<int> nbBook) async {
    Text title = const Text(
      "Payer maintenant ou continuer vos achats ?",
    );
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext ctx) {
          return (Theme.of(context).platform == TargetPlatform.iOS)
              ? CupertinoAlertDialog(
            title: title,
            actions: <Widget>[
              payBtn(ctx, dataBox, nbBook),
              close(ctx, "Continuer mes achats")
            ],
          )
              : AlertDialog(
            title: title,
            actions: <Widget>[
              payBtn(ctx, dataBox, nbBook),
              close(ctx, "Continuer mes achats")
            ],
          );
        });
  }

  TextButton payBtn(BuildContext ctx, Box<CartItem> dataBox, Box<int> nbBook) {
    return TextButton(
      onPressed: () async {
        Navigator.pop(ctx);
        Navigator.push(
            ctx,
            MaterialPageRoute(
                builder: (context) =>
                    BlocProvider<CartBloc>(
                        create: (BuildContext context) =>
                            CartBloc(),
                        child: CartView(dataBox: dataBox, nbBook: nbBook,))));
      },
      child: const Text(
        "Payer",
        style: TextStyle(color: Colors.green),
      ),
    );
  }

}