import 'package:dio/dio.dart';
import 'package:edenred/model/book.dart';
import 'package:edenred/model/cart_item.dart';
import 'package:hive/hive.dart';

class WS {
  final String baseUrl = "https://henri-potier.techx.fr/books";

  Future<List<Book>> getBooks() async {
    Dio dio = Dio();

    Response response = await dio.get(
      baseUrl,
      options: Options(
        headers: {
          'Accept': "application/json"
        },
      ),
    );

    List<Book> books = [];
    for(var i=0; i< response.data.length; i++){
      books.add(Book.fromJson(response.data[i]));
    }
    return books;
  }

  Future<Map<String, dynamic>> getDataCart() async{
    Iterable list = Hive.box<CartItem>('cart').values;
    String isdns = "";
    int totalPrice = 0;
    int qty = 0;
    for (var element in list) {
      CartItem cartItem = element;
      qty += cartItem.qty;
      totalPrice += cartItem.price! * qty;
      isdns += isdns == "" ? cartItem.isbn! : ","+cartItem.isbn!;
      if(qty > 1){
        for(var i=1; i < qty; i++){
          isdns +=","+cartItem.isbn!;
        }
      }
    }

    Dio dio = Dio();

    Response response = await dio.get(
      baseUrl + "/" + isdns + "/commercialOffers",
      options: Options(
        headers: {
          'Accept': "application/json"
        },
      ),
    );

    double bestPrice = 0;
    for(var i= 0; i< response.data["offers"].length; i++){
      bestPrice = switchPromo(totalPrice, response.data["offers"][i]);
    }

    Map<String, dynamic> map = {
      "nb":qty,
      "total_price":totalPrice - bestPrice,
      "discount":bestPrice
    };
    return map;
  }

  double switchPromo(int totalPrice, Map<String, dynamic> map){
    double bestPrice = 0;
    switch(map["type"]){
      case "percentage":
        {
          if (totalPrice * map["value"] / 100 >
              bestPrice) {
            bestPrice = totalPrice * map["value"] / 100;
          }
        }
        break;
      case "minus":
        {
          if (map["value"] > bestPrice) {
            bestPrice = (map["value"]).toDouble();
          }
        }
        break;
      case "slice":
        {
          if ((totalPrice / map["sliceValue"]).truncate() * map["value"] > bestPrice) {
            bestPrice = ((totalPrice / map["sliceValue"]).truncate() * map["value"]).toDouble();
          }
        }
        break;
      default:
        break;
    }
    return bestPrice;
  }

}
