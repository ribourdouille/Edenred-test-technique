// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:edenred/utils/ws.dart';
import 'package:test/test.dart';

void main() {
  test('The result of slice need to be 15',(){
    int total = 110;
    Map<String, dynamic> map = {
      "type": "slice",
      "sliceValue": 100,
      "value": 15
    };
    expect(WS().switchPromo(total, map),15);
  });
}
